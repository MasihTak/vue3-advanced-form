# vue3-advanced-form
Advanced and yet reusable form with real-time validation feedback written in Vue 3.

## Features
- Reusable form
- Reactive
- Reusable components (currently only text)
- Real-time validation!

## Project Demo
https://vue3-advanced-form.vercel.app/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Donation
If you see this project as useful please consider some donations which lead to more cool and useful projects like this!

[PayPal](https://www.paypal.com/paypalme/masihabjadi)

## Sponsor
[![JetBrains Logo](jetbrains.svg)](https://www.jetbrains.com/?from=https://gitlab.com/MasihTak/vue3-advanced-form)
