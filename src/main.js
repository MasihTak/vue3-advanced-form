import { createApp } from 'vue'
import App from './App.vue'

const app = createApp(App);

// Register custom directive for the focus state
app.directive('focus', {
    // When the bound element is mounted into the DOM...
    mounted(el, binding) {
        if(binding.value === true) {
            // Focus the element
            el.focus()
        }
    }
})

app.mount('#app');
